#include <iostream>
#include <fstream>
#include <windows.h>
#include <cstdlib>
#include <cstdio>
#include <string>
#include <conio.h>
#include <time.h>
#include <algorithm>

using namespace std;

string what;
string corr, inp;
fstream plik;
int wynik;

int main()
{
    plik.open("quiz.jp2", ios::in);
    if(!plik.good()) {
        cout << "Plik quiz.jp2 nie istnieje!\n";
        system("pause");
        exit(2137);
    }

    getline(plik, what);
    cout << "Tytul quizu: " << what << endl;
    getline(plik, what);
    cout << "Autor quizu: " << what << "\n\n";

    for(int i = 1; i <= 5; ++i) {
        getline(plik, what);
        cout << i << ". " << what << endl;
        getline(plik, what);
        cout << "a. " << what << endl;
        getline(plik, what);
        cout << "b. " << what << endl;
        getline(plik, what);
        cout << "c. " << what << endl;
        getline(plik, what);
        cout << "d. " << what << endl;
        getline(plik, corr);
        cout << "Twoja odpowiedz: "; cin >> inp;
        transform(inp.begin(), inp.end(), inp.begin(), ::tolower);
        if (inp == corr)
        {
            cout << "Dobrze! Zdobywasz punkt!\n\n\n";
            wynik++;
        }
        else
        {
            cout << "Zle! Poprawna odpowiedz to: " << corr << "\n\n\n";
        }

    }

    cout << "Twoj wynik: " << wynik << "/5\n\n";
    system("pause");

    return 0;
}
