#include <stdio.h>
#include <string.h> // TODO: get rid of this header

FILE  *fquiz;
char  cline[512]; // TODO: Make a dynamic allocation thingy to save memory as 512B is quite a lot.
char  *canswer;   //TODO: Make this into one static CHAR, not a pointer.
char  cuseraw[1];
int   dresult; // TODO: Use 8-bit integer instead as we only need 5 max value.

char* cgfl() { return fgets ( cline, sizeof ( cline ), fquiz ); }

int main ( ) {
  fquiz = fopen( "quiz.jp2", "r" ); // TODO: Error handling on this
  printf ( "Tytul quizu: %s", cgfl ( ) );
  printf ( "Autor quizu: %s\n", cgfl ( ) );

  for ( int i = 1 ; i <= 5 ; ++i ) {
    printf ( "%d. %s", i, cgfl ( ) ); // TODO: Do something to prevent slowdowns if disk I/O times are bad.

    printf ( "a. %s", cgfl ( ) );
    printf ( "b. %s", cgfl ( ) );
    printf ( "c. %s", cgfl ( ) );
    printf ( "d. %s", cgfl ( ) );

    printf ( "Twoja odpowiedz: " );

    canswer = cgfl(); // TODO: This uses a massive amount of allocated memory (512B),
                      //  make this 1B for this specific function without sacrificing stability and extensibility.

    scanf  ( "%s", cuseraw ); // TODO: Case-insensitive // Read only 1 CHAR
    
    if ( strncmp ( canswer, cuseraw, 1 ) == 0 ) { // TODO: get rid of this to get rid of string.h to save CPU cycles.
      printf ( "\nDobrze! Zdobywasz punkt!\n" );
      dresult++;
    } else {
      printf ( "\nZle! Poprawna odpowiedz to: %s\n", canswer );
    }
      
      
  }

  fclose ( fquiz );
  
  printf ( "\nTwoj wynik: %d/5\n\n", dresult );
  
  return dresult;
}
