﻿Imports System.IO : Imports System.Console
Module Module1

    Dim fiel As String()
    Dim what As String
    Dim line As Integer = 0
    Dim corr As String
    Dim inpt As String
    Dim wynik As Integer = 0
    Sub Main()
        Try
            fiel = File.ReadAllLines("quiz.jp2")
        Catch ex As Exception
            WriteLine("Plik quiz.jp2 nie istnieje!")
            ReadKey()
            Environment.Exit(2137)
        End Try

        WriteLine("Tytul quizu: " + fiel(line))
        line = line + 1
        WriteLine("Autor quizu: " + fiel(line) + vbLf + vbLf)
        line = line + 1
        For I = 1 To 5
            WriteLine(I.ToString() + ". " + fiel(line))
            line = line + 1
            WriteLine("a. " + fiel(line))
            line = line + 1
            WriteLine("b. " + fiel(line))
            line = line + 1
            WriteLine("c. " + fiel(line))
            line = line + 1
            WriteLine("d. " + fiel(line))
            line = line + 1
            corr = fiel(line)
            line = line + 1
            Write("Twoja odpowiedz: ")
            inpt = ReadLine()
            If inpt = corr Then
                WriteLine("Dobrze! Zdobywasz punkt!" + vbLf + vbLf + vbLf)
                wynik = wynik + 1
            Else
                WriteLine("Żle! Poprawna odpowiedz to: " + corr.ToString() + vbLf + vbLf + vbLf)
            End If
        Next

        WriteLine("Twoj wynik: " + wynik.ToString() + "/5")
        ReadKey()
        Environment.Exit(0)
    End Sub

End Module
